nb960 <- function(pieces960) {
  
  pieces960 <- tolower(pieces960)
  
  bishops <-
    lapply(
      sapply(pieces960, gregexpr, pattern = "b"),
      function(x) {
        y <- if (bitwAnd(x[1], 1)) rev(x) else x
        c(y[1] / 2 - 1, (y[2] - 1) / 2)
      }
    )
  x1 <- sapply(bishops, `[`, 1)
  x2 <- sapply(bishops, `[`, 2)
  
  without_b <- gsub("b", "", pieces960)
  x3 <- sapply(without_b, regexpr, pattern = "q") - 1
  
  without_bq <- sub("q", "", without_b)
  code_krn <-
    c(nnrkr = 0, nrnkr = 1, nrknr = 2, nrkrn = 3, rnnkr = 4,
      rnknr = 5, rnkrn = 6, rknnr = 7, rknrn = 8, rkrnn = 9)
  x4 <- unname(code_krn[without_bq])
  
  if (anyNA(x4)) stop("wrong chess 960 position")
  
  as.integer(x1 + 4 * x2 + 16 * x3 + 96 * x4)
  
}

pieces960 <- function(pos_nb) {

  free <- function(occup) lapply(occup, setdiff, x = 1:8)
  
  b1 <- pos_nb %% 4
  n2 <- pos_nb %/% 4
  b2 <- n2 %% 4
  n3 <- n2 %/% 4
  
  colb1 <- (b1 + 1) * 2
  colb2 <- (b2 * 2) + 1
  
  q <- n3 %% 6
  n4 <- n3 %/% 6
  
  colq <- mapply(
    function(x, y) x[y],
    free(Map(c, colb1, colb2)),
    q + 1
  )
  
  coln <- Map(
    function(x, y) c(x[y[1]], x[y[2]]),
    free(Map(c, colb1, colb2, colq)),
    lapply(n4 + 1, function(x) combn(5, 2)[ , x])
  )
  
  coln1 <- sapply(coln, `[`, 1)
  coln2 <- sapply(coln, `[`, 2)
  
  last3 <- free(Map(c, colb1, colb2, colq, coln1, coln2))
  
  colr1 <- sapply(last3, `[`, 1)
  colk <-  sapply(last3, `[`, 2)
  colr2 <- sapply(last3, `[`, 3)
  
  mapply(
    function(b1, b2, q, n1, n2, r1, r2, k) {
      res <- strrep("-", 8)
      substr(res, b1, b1) <- "B"
      substr(res, b2, b2) <- "B"
      substr(res,  q,  q) <- "Q"
      substr(res, n1, n1) <- "N"
      substr(res, n2, n2) <- "N"
      substr(res, r1, r1) <- "R"
      substr(res, r2, r2) <- "R"
      substr(res,  k,  k) <- "K"
      res
    },
    colb1, colb2, colq, coln1, coln2, colr1, colr2, colk
  )
  
}


# FEN8 <-
#   stringr::str_match(
#     readLines("Online_Chess_960.pgn"),
#     "\\[FEN \"([rnbkqp]+)/.+"
#   )
# ligne8 <- FEN8[ , 2][complete.cases(FEN8)]
# 
# nb960(ligne8)
# 
# (ech <- sample(0:959, 5))
# pieces960(ech) -> test
# nb960(test)
