module Chess960(id960, fen960) where

import Data.List (elemIndices, delete, (\\), sortBy)
import Data.List.Split (splitOn)
import Data.Function (on)
import qualified Data.Tuple as Tuple (swap)
import Data.Maybe (fromMaybe)
import Data.Char (toUpper)

krnstr :: [(Int, String)]
krnstr =
  [(0, "nnrkr"),
   (1, "nrnkr"),
   (2, "nrknr"),
   (3, "nrkrn"),
   (4, "rnnkr"),
   (5, "rnknr"),
   (6, "rnkrn"),
   (7, "rknnr"),
   (8, "rknrn"),
   (9, "rkrnn")]

krnint :: [(String, Int)]
krnint = map Tuple.swap krnstr

colb :: String -> [Int]
colb pieces8 =
  let findb = elemIndices 'b' pieces8
  in if even $ findb !! 0 then findb else reverse findb

id960 :: String -> Int
id960 fen = coeffb1 + 4 * coeffb2 + 16 * coeffq + 96 * coeffkrn
  where pieces8 = splitOn "/" fen !! 0
        pieces6 = filter (/='b') pieces8
        pieces5 = delete 'q' pieces6
        coeffb1 = (colb pieces8 !! 1) `div` 2
        coeffb2 = (colb pieces8 !! 0) `div` 2
        coeffq = (elemIndices 'q' pieces6) !! 0
        coeffkrn = fromMaybe (-100) $ lookup pieces5 krnint
        
-- id960 "nrbbkqrn/pppppppp/8/8/8/8/PPPPPPPP/NRBBKQRN"
-- map id960 ["rnbqkbnr", "rnbkqbnr", "nrbbkqrn", "bbqnnrkr"]

full_fen :: String -> String
full_fen lig8 =
  let vide = "8/8/8/8/" ; rep8 = replicate 8 ; upcase = map toUpper
  in lig8 ++ "/" ++ rep8 'p' ++ "/" ++ vide ++ rep8 'P' ++ "/" ++ upcase lig8

libres :: [Int] -> [Int]
libres occup = [1..8] \\ occup

-- couples (colonne, piece)
pos960 n =
  let b1 = n `mod` 4
      n2 = n `div` 4
      b2 = n2 `mod` 4
      n3 = n2 `div` 4
      colb1 = (b1 + 1) * 2
      colb2 = (b2 * 2) + 1
      q = n3 `mod` 6
      colq = (libres [colb1, colb2]) !! q
      posbq = [(colb1, 'b'), (colb2, 'b'), (colq, 'q')]
      libres5 = libres [colb1, colb2, colq]
      n4 = n3 `div` 6
      ordkrn = fromMaybe "#####" $ lookup n4 krnstr
      poskrn = zip libres5 ordkrn
  in poskrn ++ posbq

-- position complete
fen960 n =
  let pos_str = snd . unzip . sortBy (compare `on` fst)
  in full_fen $ pos_str (pos960 n)
